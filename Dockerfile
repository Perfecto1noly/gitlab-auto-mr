ARG PYTHON_VERSION=3.11

FROM python:${PYTHON_VERSION}-alpine as builder

WORKDIR /build

COPY ./ ./
RUN python setup.py sdist

FROM python:${PYTHON_VERSION}-alpine as final
LABEL MAINTAINER="Haseeb Majid <me@haseebmajid.com>"

COPY --from=builder /build/dist/ ./dist/
RUN pip install dist/*
